package com.sobel_worker.sobel_worker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SobelWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SobelWorkerApplication.class, args);
	}

}
